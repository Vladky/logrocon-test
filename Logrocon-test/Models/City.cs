﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Logrocon_test.Models
{
    public class CitiesContext: DbContext
    {
        public DbSet<City> Cities { get; set; }
    }
    public class City
    {
        public int Id { get; set; }
        [Display(Name="Название")]
        public string Name { get; set; }
        [Display(Name = "Население")]
        public int Population { get; set; }
        [Display(Name = "Областной центр")]
        public bool IsCenter { get; set; }
    }
}