﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Logrocon_test.Startup))]
namespace Logrocon_test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
