namespace Logrocon_test.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<Logrocon_test.Models.CitiesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Logrocon_test.Models.CitiesContext context)
        {
            context.Cities.AddOrUpdate(
                c => c.Name,
                new City { Name = "Moscow", Population = 11514330, IsCenter = true },
                new City { Name = "Saint-Petersburg", Population = 4848742, IsCenter = true },
                new City { Name = "Novosibirsk", Population = 1498921, IsCenter = true },
                new City { Name = "Ekaterinburg", Population = 1377738, IsCenter = true },
                new City { Name = "Surgut", Population = 306703, IsCenter = false },
                new City { Name = "Sterlitamak", Population = 273432, IsCenter = false },
                new City { Name = "Nizhnevartovsk", Population = 251860, IsCenter = false },
                new City { Name = "Nefteyugansk", Population = 123276, IsCenter = false });
        }
    }
}
